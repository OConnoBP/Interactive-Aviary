﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdInteraction : MonoBehaviour {
	public GameObject bird;
	public bool hasInteracted;
	public float emoteTime = 3f;
	public Sprite emote;
	public Light light;
	public GameObject imgBtn;
	// Use this for initialization
	void Start () {
		bird = this.gameObject;
		hasInteracted = false;
		//emote = Resources.Load<Sprite>("angry emo2");
		//gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = emote;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.tag == "Player") {
			this.light.enabled = true;
			if (hasInteracted == false) {
				imgBtn.gameObject.SetActive (true);
			}
		}
	}

	void OnTriggerStay(Collider other){
		if (other.gameObject.tag == "Player") {
			if (Input.GetButtonDown("Fire1")) {
				StartCoroutine(ShowEmote ());
				hasInteracted = true;
			}
			if (hasInteracted == false) {
				imgBtn.gameObject.SetActive (true);
			} else if (hasInteracted == true) {
				imgBtn.gameObject.SetActive (false);
			}
		}

	}

	IEnumerator ShowEmote(){
		gameObject.transform.GetChild (0).gameObject.SetActive (true);
		yield return new WaitForSeconds (emoteTime);
		gameObject.transform.GetChild (0).gameObject.SetActive (false);
	}

	void OnTriggerExit(Collider other){
		if (other.gameObject.tag == "Player") {
			this.light.enabled = false;
			imgBtn.gameObject.SetActive (false);
		}
	}
}
