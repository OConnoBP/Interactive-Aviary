﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	public float speed = 6.0f;
	private CharacterController _characterController;
	private Animator animator;
	private Vector3 dir;

	// Use this for initialization
	void Start () {
		this._characterController = GetComponent<CharacterController> ();
		animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		dir = Vector3.zero;
		dir.x = Input.GetAxis ("Horizontal");
		float deltaX = Input.GetAxis ("Horizontal") * .75f;
		float deltaZ = Input.GetAxis ("Vertical") * this.speed;
		Vector3 movement = new Vector3 (deltaX, 0, deltaZ);
		movement = Vector3.ClampMagnitude (movement, speed);

		movement *= Time.deltaTime;
		movement = transform.TransformDirection (movement);
		_characterController.Move (movement);

		if (dir != Vector3.zero) {
			transform.Rotate (0, Input.GetAxis ("Horizontal") * .75f, 0);
		}

		if (Input.GetAxis ("Horizontal") != 0 || Input.GetAxis ("Vertical") != 0) {
			animator.SetTrigger ("scarlet_walk");
		} else {
			animator.Play ("idle");
		}
	}
}
