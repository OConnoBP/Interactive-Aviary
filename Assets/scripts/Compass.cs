﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Compass : MonoBehaviour {
	Vector3 dir;
	float angle;
	public GameObject arrow;
	public GameObject finalText;
	public GameObject target;
	public GameObject closestbird;
	public float closestdistance;
	public float pointer;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		closestdistance = Mathf.Infinity;
		closestbird = null;
		int numInteracted = 0;
		foreach (var bird in GameObject.FindGameObjectsWithTag("Bird")) {
			if (bird.GetComponent<BirdInteraction> ().hasInteracted == true) {
				numInteracted++;
			}
		}
		Debug.Log (GameObject.FindGameObjectsWithTag ("Bird").Length);
		if (numInteracted != 4) {
			foreach (var bird in GameObject.FindGameObjectsWithTag("Bird")) {
				if (bird.GetComponent<BirdInteraction> ().hasInteracted == false) {
					if (Vector3.Distance (bird.transform.position, transform.position) < closestdistance) {
						closestbird = bird;
						closestdistance = Vector3.Distance (bird.transform.position, transform.position);
					}
				}
			}
			target = closestbird;

			pointer = GameObject.FindGameObjectWithTag("Pointer").transform.localEulerAngles.y;
			if (pointer < 0) {
				pointer = 360 + pointer;
			}
			arrow.transform.eulerAngles = new Vector3(0, 0, -pointer);
		} else{
			arrow.SetActive (false);
			finalText.SetActive (true);
			GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerValues> ().isDone = true;
		}



	}
}
