﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IbisAnimations : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (GetComponent<wander> ().paused == true) {
			GetComponent<Animator> ().Play ("idle");
		} else {
			GetComponent<Animator> ().SetTrigger ("scarlet_walk");
		}
	}
}
