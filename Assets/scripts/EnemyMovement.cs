﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {
	Transform target;
	public float moveSpeed = 3f;
	public float rotationSpeed = 3f;
	Transform myTransform;
	public bool angery = false;
	void Awake(){
		myTransform = transform;
	}
	// Use this for initialization
	void Start () {
		target = GameObject.FindWithTag ("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {
		if (angery != true) {
			//Debug.Log ("Stay out!");
		}
	}

	public void Angery(){
		angery = true;
		myTransform.rotation = Quaternion.Slerp (myTransform.rotation, Quaternion.LookRotation (target.position - myTransform.position), rotationSpeed * Time.deltaTime);
		myTransform.position += myTransform.forward * moveSpeed * Time.deltaTime;
	}
}
