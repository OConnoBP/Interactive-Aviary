﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdTerritory : MonoBehaviour {
	public EnemyMovement enemy;

	void OnTriggerStay(Collider other){
		if (tag.Equals ("EnemyTerritory") && other.gameObject.tag == "Player") {
			enemy.Angery ();
		} else if(tag.Equals("FriendlyTerritory") && other.gameObject.tag == "Player"){

		}
	}

	void OnTriggerExit(Collider other){
		if (tag.Equals ("EnemyTerritory") && other.gameObject.tag == "Player") {
			enemy.angery = false;
		}
	}

	IEnumerator Narration(){
		Time.timeScale = 0.0f;
		yield return new WaitForSecondsRealtime (5);
		Time.timeScale = 1.0f;
	}
}
