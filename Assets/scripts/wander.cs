﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wander : MonoBehaviour {
	public float speed = 1;
	public float directionChangeInterval = 10;
	public float maxHeadingChange = 360;
	public bool paused = false;
	CharacterController controller;
	public float heading;
	private Vector3 targetRotation;
	public Transform origin;
	private Vector3 myPosition;
	public bool isOutOfBounds = false;
	private bool initialDirSet = false;
	Animator animator;

	void Awake(){
		animator = GetComponent<Animator> ();
		controller = GetComponent<CharacterController> ();

		heading = Random.Range (0, 360);
		transform.eulerAngles = new Vector3 (0, heading, 0);

		StartCoroutine (NewHeading ());
	}
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (GetComponent<EnemyMovement> ()) {
			if (GetComponent<EnemyMovement> ().angery == false) {
				myPosition = GetComponent<Transform> ().transform.position;
				if (paused == false) {
					//if within range
					if (Vector3.Distance (myPosition, origin.position) <= 10) {
						isOutOfBounds = false;
						transform.eulerAngles = Vector3.Slerp (transform.eulerAngles, targetRotation, Time.deltaTime * directionChangeInterval);
						var forward = transform.TransformDirection (Vector3.forward);
						controller.SimpleMove (forward * speed);
					} else {
						isOutOfBounds = true;
						transform.eulerAngles = Vector3.Slerp (transform.eulerAngles, targetRotation, Time.deltaTime * directionChangeInterval);
						var forward = transform.TransformDirection (Vector3.forward);
						controller.SimpleMove (forward * speed);
					}
					//else, turn back to the origin
				} else {

				}
			} else {

			}
		} else {
			myPosition = GetComponent<Transform> ().transform.position;
			if (paused == false) {
				//if within range
				if (Vector3.Distance (myPosition, origin.position) <= 10) {
					isOutOfBounds = false;
					transform.eulerAngles = Vector3.Slerp (transform.eulerAngles, targetRotation, Time.deltaTime * directionChangeInterval);
					var forward = transform.TransformDirection (Vector3.forward);
					controller.SimpleMove (forward * speed);
				} else {
					isOutOfBounds = true;
					transform.eulerAngles = Vector3.Slerp (transform.eulerAngles, targetRotation, Time.deltaTime * directionChangeInterval);
					var forward = transform.TransformDirection (Vector3.forward);
					controller.SimpleMove (forward * speed);
				}
				//else, turn back to the origin
			}
		}



	}

	IEnumerator NewHeading(){
		while (true) {
			NewHeadingRoutine ();
			yield return new WaitForSeconds (directionChangeInterval);
			StartCoroutine (Pause ());
		}
	}

	IEnumerator Pause(){
		paused = true;
		yield return new WaitForSeconds (5);
		paused = false;
	}

	void NewHeadingRoutine(){
		if (isOutOfBounds == false) {
			initialDirSet = false;
			var floor = Mathf.Clamp (heading - maxHeadingChange, 0, 360);
			var ceil = Mathf.Clamp (heading + maxHeadingChange, 0, 360);
			heading = Random.Range (floor, ceil);
			targetRotation = new Vector3 (0, heading, 0);
		} else {
			if (initialDirSet == false) {
				if (transform.rotation.eulerAngles.y >= 180) {
					heading = Mathf.Abs (transform.rotation.eulerAngles.y - 180);
				} else {
					heading = 360 - Mathf.Abs (transform.rotation.eulerAngles.y - 180);
				}
				initialDirSet = true;
			}
			targetRotation = new Vector3 (0, heading, 0);
		}
	}
}
