﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuckAnimations : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (GetComponent<wander> ().paused == true) {
			GetComponent<Animator> ().Play ("idle");
		} else {
			if (GetComponent<EnemyMovement> ().angery == true) {
				GetComponent<Animator> ().Play ("attack");
			} else {
				GetComponent<Animator> ().SetTrigger ("duck_walk");
			}
		}
	}
}
