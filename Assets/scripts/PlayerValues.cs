﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerValues : MonoBehaviour {
	public bool isIdle;
	public bool isDone;
	private bool _idleCoroutine;
	private bool _playerMoved;

	// Use this for initialization
	void Start () {
		isIdle = false;
		isDone = false;
		_idleCoroutine = false;
		_playerMoved = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetAxis ("Horizontal") != 0 || Input.GetAxis ("Vertical") != 0) {
			isIdle = false;
			_playerMoved = true;
		} else if (Input.GetAxis ("Horizontal") == 0 && Input.GetAxis ("Vertical") == 0) {
			if (!_idleCoroutine) {
				StartCoroutine (Idle ());
			}
		}
	}

	IEnumerator Idle(){
		_idleCoroutine = true;
		_playerMoved = false;
		yield return new WaitForSeconds (20);
		if (!_playerMoved) {
			isIdle = true;
		}
		_idleCoroutine = false;
	}
}