﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// The purpose of the billboard class is to make sprites always be facing the camera
public class Billboard : MonoBehaviour {
	// Update is called once per frame
	void Update () {
		transform.LookAt (Camera.main.transform.position, Vector3.up);
	}
}
