﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Bird : MonoBehaviour {
	public abstract void Interact();
	public abstract void Behavior();
}
