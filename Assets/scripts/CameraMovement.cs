﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CameraMovement : MonoBehaviour {
	public float activeSize = 10;
	public float inactiveSize = 5;
	private Camera _camera;
	private float aTimer = 0.0f;
	public GameObject btn;
	public GameObject text;
	private bool isZoomingIn = false;
	private bool isZoomingOut = false;
	public GameObject compass;

	// The player object is used to determine whether or not the player is idle
	private GameObject player;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		this._camera = GetComponent<Camera> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!player.GetComponent<PlayerValues> ().isDone) {
			// If the player is idle
			if (player.GetComponent<PlayerValues> ().isIdle) {
				// If the camera is not already zooming in, set the timer to 0
				if (!isZoomingIn) {
					aTimer = 0.0f;
				}

				if (_camera.orthographicSize != inactiveSize) {
					// Camera should always be zooming in if current size != inactive size
					isZoomingIn = true;
					CameraZoomIn ();
				} else {
					// Once the camera has reached inactive size, it is no longer zooming in
					isZoomingIn = false;
				}
				// If the player is not idle
			} else {
				isZoomingIn = false;
				// If the camera is not already zooming out, set the timer to 0
				if (!isZoomingOut) {
					aTimer = 0.0f;
				}

				if (_camera.orthographicSize != activeSize) {
					// Camera should always be zooming out if the current size != active size
					isZoomingOut = true;
					CameraZoomOut ();
				} else {
					// Once the camera has reached active size, it is no longer zooming out
					isZoomingOut = false;
				}
			}

			if (_camera.orthographicSize == activeSize) {
				isZoomingOut = false;
			} else if (_camera.orthographicSize == inactiveSize) {
				isZoomingIn = false;
			}

			if (_camera.orthographicSize == inactiveSize) {
				btn.SetActive (true);
				text.SetActive (true);
				compass.SetActive (false);
				if (Input.GetButtonDown ("Fire1")) {
					SceneManager.LoadScene ("Test", UnityEngine.SceneManagement.LoadSceneMode.Single);
				}
			} else {
				btn.SetActive (false);
				text.SetActive (false);
				compass.SetActive (true);
			}
		} else {
			CameraZoomIn ();
			btn.SetActive (true);
			if (Input.GetButtonDown ("Fire1")) {
				SceneManager.LoadScene ("Test", UnityEngine.SceneManagement.LoadSceneMode.Single);
			}
		}

	}

	void CameraZoomIn(){
		isZoomingIn = true;
		aTimer += Time.deltaTime;
		this._camera.orthographicSize = Mathf.Lerp (activeSize, inactiveSize, aTimer);
	}

	void CameraZoomOut(){
		isZoomingOut = true;
		aTimer += Time.deltaTime;
		this._camera.orthographicSize = Mathf.Lerp (inactiveSize, activeSize, aTimer);
	}
}
