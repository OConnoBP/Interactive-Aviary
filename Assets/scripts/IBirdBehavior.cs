﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBirdBehavior {
	void doStuff();
	void OnTriggerStay ();
	void Movement();
}
