﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class faceforward : MonoBehaviour {

	Quaternion quaternion;
	Vector3 position;
	// Use this for initialization
	void Start () {
		quaternion = transform.rotation;
		position = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
		transform.rotation = quaternion;
		transform.localPosition = position;
	}
}
